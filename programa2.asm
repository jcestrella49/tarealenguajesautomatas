;Juan Carlos Estrella Rodriguez
.model small
.stack 64
.data
include 'emu8086.inc'
msj1 db 0ah,0dh, 'Ingrese 3 numeros : ', '$'
msj2 db 0ah,0dh, 'Primer numero: ', '$'
msj3 db 0ah,0dh, 'Segundo numero: ', '$'
msj4 db 0ah,0dh, 'Tercer numero: ', '$'
Mayor db 0ah,0dh, 'El mayor es: ', '$'

numero1 db 100 dup('$')
numero2 db 100 dup('$')
numero3 db 100 dup('$')

salto db 13,10,'',13,10,'$' 
.code
inicio:
mov si,0
mov ax,@data
mov ds,ax
mov ah,09
mov dx,offset msj1 
int 21h

call saltarLinea

call soliNum

mov numero1,al 

call saltarLinea

call soliNum

mov numero2,al

call saltarLinea

call soliNum

mov numero3,al

call saltarLinea

mov ah,numero1
mov al,numero2
cmp ah,al ;
ja comparar-1-3 
jmp comparar-2-3 
comparar-1-3:
mov al,numero3
cmp ah,al 
ja mayor1 

comparar-2-3:
mov ah,numero2
mov al,numero3
cmp ah,al 
ja mayor2 
jmp mayor3 

 
mayor1:

call msjMay 

mov dx, offset numero1
mov ah, 9
int 21h
jmp exit

mayor2:
call msjMay

mov dx, offset numero2 
mov ah, 9
int 21h
jmp exit

mayor3:
call msjMay

mov dx, offset numero3 
mov ah, 9
int 21h
jmp exit



msjMay:
mov dx, offset Mayor 
mov ah, 9
int 21h

ret
soliNum:
mov ah,01h
int 21h
ret

saltarLinea:
mov dx, offset salto 
mov ah, 9
int 21h
ret

exit:
mov ax, 4c00h
int 21h 
ends